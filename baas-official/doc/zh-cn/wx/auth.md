# 微信网页授权登录

初始化
引入OAuth并实例化

```js
const client = new module.WechatOauth('your appid', 'your secret');
```

以上即可满足单进程使用。 当多进程时，token需要全局维护，以下为保存token的接口。

```js
const oauthApi = new module.WechatOauth('appid', 'secret', async function (openid) {
  // 传入一个根据openid获取对应的全局token的方法 
  return await fse.readJson(openid +':access_token.txt', 'utf8');
}, async function (openid, token) {
  // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等 
  // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例 
  // 持久化时请注意，每个openid都对应一个唯一的token! 
  await fse.outputJson(openid + ':access_token.txt', token);
});
```

引导用户
生成引导用户点击的URL。

```js
const url = client.getAuthorizeURL('redirectUrl', 'state', 'scope');
```

如果是PC上的网页，请使用以下方式生成

```js
const url = client.getAuthorizeURLForWebsite('redirectUrl');
```

获取Openid和AccessToken
用户点击上步生成的URL后会被重定向到上步设置的 redirectUrl，并且会带有code参数，我们可以使用这个code换取access_token和用户的openid

```js
const token = await client.getAccessToken('code');
const accessToken = token.data.access_token;
const openid = token.data.openid;
```

获取用户信息
如果我们生成引导用户点击的URL中scope参数值为snsapi_userinfo，接下来我们就可以使用openid换取用户详细信息（必须在getAccessToken方法执行完成之后）

```js
const userInfo = await client.getUser('openid');
```
