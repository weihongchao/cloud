# 模版消息

------

##### 获取小程序模板库标题列表

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| offset |  是 |   offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。|
| count | 是 |   offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。|

```js
const getTemplates = await wxa.getTemplates( offset, count );
```

##### 获取模板库某个模板标题下关键词库

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| id |  是 |   模板标题id，可通过接口获取，也可登录小程序后台查看获取|

```js
const getKeywordTemplates = await wxa.getKeywordTemplates( id );
```

##### 组合模板并添加至帐号下的个人模板库

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| id |  是 | 模板标题id，可通过接口获取，也可登录小程序后台查看获取 |
| keywordIdList | 是 | 开发者自行组合好的模板关键词列表，关键词顺序可以自由搭配（例如[3,5,4]或[4,5,3]），最多支持10个关键词组合 |

```js
const addTemplate = await wxa.addTemplate( id, keywordIdList );
```

##### 获取帐号下已存在的模板列表

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| offset |  是 |offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。最后一页的list长度可能小于请求的count|
| count | 是 |offset和count用于分页，表示从offset开始，拉取count条记录，offset从0开始，count最大为20。最后一页的list长度可能小于请求的count|

```js
const getPrivateTemplates = await wxa.getPrivateTemplates( offset, count );
```

##### 删除帐号下的某个模板

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| id |  是 | 要删除的模板id |

```js
const delPrivateTemplate = await wxa.delPrivateTemplate( id );
```

##### 发送模板消息

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| openid |  是 | 接收者（用户）的 openid |
| templateId |  是 | 所需下发的模板消息的id |
| page |  否 | 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。 |
| formId |  是 | 表单提交场景下，为 submit 事件带上的 formId；支付场景下，为本次支付的 prepay_id |
| data |  是 | 模板内容，不填则下发空模板 |
| color | 否 | 模板内容字体的颜色，不填默认黑色 |
| emphasisKeyword | 否 | 模板需要放大的关键词，不填则默认无放大 |

```js
const sendTemplate = await wxa.sendTemplate({
    openid: openid,
    templateId: templateId,
    page: page,
    formId: formId,
    color: color,
    data: data,
    emphasisKeyword: emphasisKeyword
  });
```