# 常见问题

1.appid和appkey通过请求头传递，无法设置请求头是怎么办？

> 无法设置请求头时，可以通过url传递，例如 `x-qingful-appid=xxx&x-qingful-appkey=xxx`

2.生成环境默认开启了缓存，如果是开发情况下，请开启开发者模式

> 错误处理：请求头部或者链接添加 `x-qingful-dev = true`

3.模糊查询时请求不到相应的数据或返回的数据为空，如 `url = '&where=name,like,%' + this.search.key + '%'`，其中`%`后面的`this.search.key`查询字段将会被转义。

>错误处理：1.`url = '&where=name,like,%\\' + this.search.key + '%'`，将`%`后面的查询字段使用`\\`转义
>2.`url = '&where=name,like,%%' + this.search.key + '%%'`，在`%`后面多加一个`%`。