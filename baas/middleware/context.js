const _ = require("lodash");
const util = require("./../util/util");
const context = require("./../util/context");

module.exports = () => {
  return async (ctx, next) => {
    // 函数
    ctx = _.assign(ctx, util);
    ctx = _.assign(ctx, context(ctx, next));

    await next();
  };
};
